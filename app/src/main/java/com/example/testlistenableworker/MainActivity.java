package com.example.testlistenableworker;

import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.Observer;
import androidx.work.Constraints;
import androidx.work.ExistingWorkPolicy;
import androidx.work.NetworkType;
import androidx.work.OneTimeWorkRequest;
import androidx.work.WorkInfo;
import androidx.work.WorkManager;

import android.os.Bundle;
import android.util.Log;

import com.example.testlistenableworker.Workers.ForegroundListenableWorker;

import java.util.List;

public class MainActivity extends AppCompatActivity {

    public static final String UNIQUE_WORKER_NAME = "UNIQUE_WORKER_NAME";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //start worker here
        Constraints constraints = new Constraints.Builder()
                .setRequiresBatteryNotLow(true)
                .setRequiredNetworkType(NetworkType.CONNECTED)
                .build();

        OneTimeWorkRequest checkDeliveryWorker = new OneTimeWorkRequest.Builder(ForegroundListenableWorker.class)
                .setConstraints(constraints)
                .build();


        WorkManager.getInstance(getApplicationContext()).getWorkInfosForUniqueWorkLiveData(UNIQUE_WORKER_NAME).observe(this, new Observer<List<WorkInfo>>() {
                    @Override
                    public void onChanged(List<WorkInfo> workInfos) {
                        if (workInfos != null) {
                            if (!workInfos.isEmpty()) {
                                WorkInfo workInfo = workInfos.get(0);
                                Log.d("WORKER_STATE",workInfo.toString());
                            }
                        }
                    }
                });


        WorkManager.getInstance(getApplicationContext()).enqueueUniqueWork(
                UNIQUE_WORKER_NAME,
                ExistingWorkPolicy.REPLACE,
                checkDeliveryWorker);

    }
}
