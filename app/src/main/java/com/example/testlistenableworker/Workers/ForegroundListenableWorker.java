package com.example.testlistenableworker.Workers;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.content.Context;
import android.os.Build;
import android.os.Handler;
import android.os.Looper;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.concurrent.futures.CallbackToFutureAdapter;
import androidx.core.app.NotificationCompat;
import androidx.work.ForegroundInfo;
import androidx.work.ListenableWorker;
import androidx.work.WorkerParameters;
import com.google.common.util.concurrent.ListenableFuture;

public class ForegroundListenableWorker extends ListenableWorker {

    private static final int NOTIFICATION_ID = 77;
    private static final  String channelId = "sample_channel_id";
    private final Handler mHandler;


    public ForegroundListenableWorker(@NonNull Context appContext, @NonNull WorkerParameters workerParams) {
        super(appContext, workerParams);
        mHandler = new Handler(Looper.getMainLooper());
    }

    @NonNull
    @Override
    public ListenableFuture<Result> startWork() {
        setForegroundAsync(createForegroundInfo(0));
        return CallbackToFutureAdapter.getFuture(new CallbackToFutureAdapter.Resolver<Result>() {
            @Nullable
            @Override
            public Object attachCompleter(@NonNull final CallbackToFutureAdapter.Completer<Result> completer) throws Exception {
                mHandler.postDelayed(new Runnable() {
                    @Override
                    public void run() {

                        ISampleCallback callback = new ISampleCallback() {
                            @Override
                            public void startSomeProcessing() {
                                setForegroundAsync(createForegroundInfo(33));
                                Utils.sleep();
                                setForegroundAsync(createForegroundInfo(100));
                                completer.set(Result.success());
                            }
                        };

                        callback.startSomeProcessing();
                    }
                },1000);
                return  "future.result";
            }
        });
    }

    @Override
    public void onStopped() {
        super.onStopped();
    }


    @NonNull
    private ForegroundInfo createForegroundInfo(@NonNull int progress) {
        Context context = getApplicationContext();
        String title = "sample_title";
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            createChannel();
        }
        Notification notification = new NotificationCompat.Builder(context, channelId)
                .setContentTitle(title)
                .setTicker(title)
                .setSmallIcon(android.R.drawable.ic_notification_overlay)
                .setProgress(100, progress,false)
                .setOngoing(true)
                .build();

        return new ForegroundInfo(NOTIFICATION_ID,notification);
    }

    @RequiresApi(Build.VERSION_CODES.O)
    private void createChannel() {
        CharSequence name = "channelName";
        String description = "channelDescription";
        int importance = NotificationManager.IMPORTANCE_HIGH;
        NotificationChannel channel =
                new NotificationChannel(channelId, name, importance);
        channel.setDescription(description);

        NotificationManager notificationManager =
                (NotificationManager) getApplicationContext().getSystemService(Context.NOTIFICATION_SERVICE);

        if (notificationManager != null) {
            notificationManager.createNotificationChannel(channel);
        }
    }


}
